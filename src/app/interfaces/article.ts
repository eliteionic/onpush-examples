export interface Article {
  headline: string;
  author: string;
  content: string;
}
