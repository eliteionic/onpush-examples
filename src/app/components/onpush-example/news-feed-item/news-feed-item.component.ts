import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  OnDestroy,
} from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { DataService } from '../../../services/data.service';
import { Article } from '../../../interfaces/article';

@Component({
  selector: 'app-news-feed-item',
  templateUrl: './news-feed-item.component.html',
  styleUrls: ['./news-feed-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewsFeedItemComponent implements OnInit, OnDestroy {
  @Input() article: Article;
  public randomNumber = 0; // Setting this way won't trigger change detection
  public randomNumber$: Observable<number> | null;

  private subscription: Subscription;

  constructor(public dataService: DataService) {}

  ngOnInit() {
    // Setting the `randomNumber` like this won't trigger change detection
    this.subscription = this.dataService
      .getRandomNumbers()
      .subscribe((value) => {
        this.randomNumber = value;
      });

    // Adding the observable directly to the template with | async pipe will trigger change detection
    this.randomNumber$ = this.dataService.getRandomNumbers();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  doSomething() {}
}
