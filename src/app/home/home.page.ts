import { Component } from '@angular/core';
import { Article } from '../interfaces/article';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public article: Article = {
    headline: '1',
    author: '1',
    content: '1',
  };

  constructor() {}

  updateArticleMutable() {
    this.article.headline = '2';
  }

  updateArticleImmutable() {
    this.article = {
      ...this.article,
      headline: '2',
    };
  }
}
